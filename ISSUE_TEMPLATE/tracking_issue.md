---
name: "FEP Tracking Issue"
about: "This template is for creating a tracking issue for a newly received FEP"
title: "[TRACKING] FEP-XXX: TITLE "
---

The proposal has been received. Thank you!

This issue tracks discussions and updates to the proposal during the `DRAFT` period.

Please post links to relevant discussions as comment to this issue.

`dateReceived`: [DATE-RECEIVED]

If no further actions are taken, the proposal will be automatically set to `WITHDRAWN` on [DATE-RECEIVED+120-DAYS] (in 120 days).

